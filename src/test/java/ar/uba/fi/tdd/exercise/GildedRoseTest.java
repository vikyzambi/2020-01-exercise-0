package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void foo() {
			Item[] items = new Item[] { new Item("fixme", 0, 0) };
			GildedRose app = new GildedRose(items);
			app.updateQuality();
			assertThat("fixme").isEqualTo(app.items[0].Name);
	}
	@Test
	public void testCalidadDeItemNuncaDecreseMasQueCero(){
		Item[] items = new Item[]{new Item("Jugo",0,0),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}
	@Test
	public void testSellInItemRandomSiempreDecrese(){
		Item[] items = new Item[]{new Item("Jugo",0,0),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(-1).isEqualTo(app.items[0].sellIn);
	}

	@Test void testCalidadDelQuesoBrieNoDecreseSinoQueAumenta(){
		Item[] items = new Item[]{new Item("Aged Brie",1,5),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(6).isEqualTo(app.items[0].quality);
	}

	@Test void testCalidadDeUnQuesoBrieNuncaSeraMasQue50(){
		Item[] items = new Item[]{new Item("Aged Brie",1,50),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(50).isEqualTo(app.items[0].quality);
	}

	@Test void testSellInQuesoBrieSiempreDecrese(){
		Item[] items = new Item[]{new Item("Aged Brie",1,50),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].sellIn);
	}


	@Test void testCalidadQuesoBrieNoDecreseCuandoLaFechaDeSellInYaPasoSinoQueAumenta2(){
		Item[] items = new Item[]{new Item("Aged Brie",-2,5),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(7).isEqualTo(app.items[0].quality);
	}

	@Test void testCalidadSulfurEsSiempre80(){
		Item[] items = new Item[]{new Item("Sulfuras, Hand of Ragnaros",1,80),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(80).isEqualTo(app.items[0].quality);
	}
	@Test void testSellINSulfurasNoDecrese(){
		Item[] items = new Item[]{new Item("Sulfuras, Hand of Ragnaros",1,80),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(1).isEqualTo(app.items[0].sellIn);
	}

	@Test void testBackstagePassCalidadAumenta2SiSellInMenorA10(){
		Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert",9,8),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(10).isEqualTo(app.items[0].quality);
	}

	@Test void testBackstagePassCalidadAumenta1SiSellIMayorA10(){
		Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert",11,8),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(9).isEqualTo(app.items[0].quality);
	}

	@Test void testBackstagePassCalidadAumenta3SiSellInMenorA5(){
		Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert",4,8),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(11).isEqualTo(app.items[0].quality);
	}

	@Test void testBackstagePassCalidadEs0SiSellInMenorA0(){
		Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert",-1,8),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test void testBackstagePassSellInSiempreDecrese(){
		Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert",9,8),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(8).isEqualTo(app.items[0].sellIn);
	}
	@Test void testCalidadDeUnBackstagePassNuncaSeraMasQue50(){
		Item[] items = new Item[]{new Item("Backstage passes to a TAFKAL80ETC concert",1,50),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(50).isEqualTo(app.items[0].quality);
	}
	@Test void testCalidadConjureDecreseDoblementeRapido(){
		Item[] items = new Item[]{new Item("Conjured",1,50),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(48).isEqualTo(app.items[0].quality);
	}

	@Test void testConjuredSellInSiempreDecrese(){
		Item[] items = new Item[]{new Item("Conjured",9,8),};
		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(8).isEqualTo(app.items[0].sellIn);
	}
}
