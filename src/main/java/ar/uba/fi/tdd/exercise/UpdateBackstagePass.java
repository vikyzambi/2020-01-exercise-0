package ar.uba.fi.tdd.exercise;

public class UpdateBackstagePass extends Update {

    final int lessThan11 = 11;
    final int lessThan6 = 6;

    @Override
    public void update(Item item) {
        if (item.quality < maxQuality) {
            item.quality = item.quality + 1;
        }
        if (item.sellIn < lessThan11) {
            if (item.quality < maxQuality) {
                item.quality = item.quality + 1;
            }
        }
        if (item.sellIn < lessThan6) {
            if (item.quality < maxQuality) {
                item.quality = item.quality + 1;
            }
        }
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < limitSellIn){
            item.quality = item.quality - item.quality;
        }

    }
}
