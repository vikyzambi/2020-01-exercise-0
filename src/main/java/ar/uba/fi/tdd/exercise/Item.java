package ar.uba.fi.tdd.exercise;

public class Item {

    public String Name;
    public int sellIn;
    public int quality;

    public Item(String _name, int _sellIn, int _quality) {
        this.Name = _name;
        this.sellIn = _sellIn;
        this.quality = _quality;
    }

   @Override
   public String toString() {

     return this.Name + ", " + this.sellIn + ", " + this.quality;
    }
 }
