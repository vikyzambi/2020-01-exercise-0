package ar.uba.fi.tdd.exercise;

public abstract class Update {
    final int maxQuality = 50;
    final int minQuality = 0;
    final int limitSellIn = 0;

    public abstract void update(Item item);
}
