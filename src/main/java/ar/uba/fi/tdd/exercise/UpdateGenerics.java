package ar.uba.fi.tdd.exercise;

public class UpdateGenerico extends Update {
    @Override
    public void update(Item item) {
        item.quality = item.quality - 1;
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < limitSellIn){
            item.quality = item.quality - 1;
        }
        if (item.quality < minQuality){
            item.quality = 0;
        }
    }
}
