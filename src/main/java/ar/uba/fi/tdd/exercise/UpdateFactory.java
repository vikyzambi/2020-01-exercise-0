package ar.uba.fi.tdd.exercise;

public class UpdateFactory {
    public Update getUpdate(String tipoProducto){
        if (tipoProducto == null){
            return null;
        }
        if  (tipoProducto.equals("Aged Brie")){
            return new UpdateBrieCheese();
        }
        else if (tipoProducto.equals("Sulfuras, Hand of Ragnaros")){
            return new UpdateSulfure();

        }
        else if (tipoProducto.equals("Backstage passes to a TAFKAL80ETC concert")){
            return new UpdateBackstagePass();
        }
        else if(tipoProducto.equals("Conjured")){
            return new UpdateConjured();
        }
        return new UpdateGenerico();

    }
}
