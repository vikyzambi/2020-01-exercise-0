
package ar.uba.fi.tdd.exercise;

class GildedRose {
    Item[] items;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    public void updateQuality() {

        UpdateFactory uf = new UpdateFactory();
        for (int i = 0; i < items.length; i++) {
            Update up =  uf.getUpdate(items[i].Name);
            if (up != null) {
                up.update(items[i]);
            }
        }
    }
}

