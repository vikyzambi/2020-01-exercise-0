package ar.uba.fi.tdd.exercise;

public class UpdateConjured extends Update {
    @Override
    public void update(Item item) {
        item.quality = item.quality - 2;
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < limitSellIn){
            item.quality = item.quality - 2;
        }
        if (item.quality < minQuality){
            item.quality = 0;
        }
    }
}
