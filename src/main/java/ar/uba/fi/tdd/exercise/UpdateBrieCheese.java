package ar.uba.fi.tdd.exercise;

public class UpdateBrieCheese extends Update {

    public void update(Item item){
        if (item.quality < maxQuality) {
            item.quality = item.quality + 1;
        }
        item.sellIn = item.sellIn - 1;
        if (item.sellIn < limitSellIn){
            if (item.quality < maxQuality) {
                item.quality = item.quality + 1;
            }
        }
    }
}